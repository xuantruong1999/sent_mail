require 'bundler'

enviroment = ENV['LBT'] || 'development'
Bundler.require(:default, enviroment)

require './lib/worker/worker'
require_relative '../app/lib/model/product'
require_relative '../app/lib/model/user'
require_relative '../app/lib/model/notifier'
require "letter_opener"
require 'action_mailer'

ActionMailer::Base.smtp_settings = {
  address:        'smtp.yourserver.com', # default: localhost
  port:           '25',                  # default: 25
  user_name:      'user',
  password:       'pass',
  authentication: :plain                 # :plain, :login or :cram_md5
}
ActiveRecord::Base.establish_connection(
  adapter:  'postgresql',
  encoding: 'unicode',
  username: 'herocore',
  password: '123456',
  database: 'activerecord'
)
ActionMailer::Base.view_paths = File.expand_path('../../app/lib/view', __FILE__)

ActionMailer::Base.add_delivery_method :letter_opener, LetterOpener::DeliveryMethod, :location => File.expand_path('../tmp/letter_opener', __FILE__)
ActionMailer::Base.delivery_method = :letter_opener
