require 'erb'
require 'byebug'
require_relative './model/email'
require_relative '../../lib/worker/worker'

class Application
  def self.call(env)
    new(env).response.finish
  end

  def initialize(env)
    @request = Rack::Request.new(env)
  end

  def response
    case @request.path
    when "/" then Rack::Response.new(render("index.html.erb"))
    when "/mail" then
      Rack::Response.new(render("done.html.erb")) do
        puts '-----------> Your job will be excuted here'
      Play.perform_async(@request.params)
      end
    else Rack::Response.new("Not Found", 404)
    end
  end

  def render(template)
    path = File.expand_path("../view/#{template}", __FILE__)
    ERB.new(File.read(path)).result(binding)
  end
end
