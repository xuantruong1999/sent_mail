require 'action_mailer'
class Notifier < ActionMailer::Base
  default from: 'system@loudthinking.com'

  def welcome(recipient)
    
    @recipient = recipient
    mail(to: recipient,
         subject: "[Signed up] Welcome #{recipient}") do | format|
           format.html
         end
  end
end
